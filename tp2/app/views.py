from django.shortcuts import render
from .models import Todo

def todos(request):
    todos = Todo.objects.filter(deleted=False)
    context = { "todos": todos }
    return render(request, 'todos.html', context)


def todo(request, pk):
    todo = Todo.objects.get(id=pk)
    context = { "todo": todo }
    return render(request, 'todo.html', context)