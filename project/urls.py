from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('afloapp/', include('afloapp.urls')),
    path('admin/', admin.site.urls),
]
