from django.shortcuts import render

def home(request):
    utilisateur = {
        "name": "Quentin",
        "age": 30,
        "formateur": True,
        "animal": {
            "name": "Strato",
            "race": "Dalmachien",
            "age": 2
        },
        "formations": ["Dev Web", "Dev IA", "Web Design"]
    }
    return render(request, 'index.html', context=utilisateur)