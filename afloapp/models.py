from django.db import models

class Formateur(models.Model):
  name = models.CharField(max_length=30)
  age = models.IntegerField(default=30)
  description = models.TextField(null=True, blank=True)

  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def __str__(self):
    return f"{self.name}"
  

class Formation(models.Model):
  name = models.CharField(max_length=20)
  number_of_students = models.IntegerField(default=0)

  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def __str__(self):
    return f"{self.name}"
  

class Etudiant(models.Model):
  name = models.CharField(max_length=30)
  age = models.IntegerField(default=18)

  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def __str__(self):
    return f"{self.name}"


class Matiere(models.Model):
  name = models.CharField(max_length=30)

  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def __str__(self):
    return f"{self.name}"
  


