from django.shortcuts import render
from .models import Formateur

def home(request):
    context = {
        "name": "Quentin",
        "age": 30,
        "is_single": False,
        "activities": [
            {
                "name": "Programming",
                "ranking": 3
            },
            {
                "name": "Gaming",
                "ranking": 4
            },
            {
                "name": "Studying",
                "ranking": 1
            }
        ]
    }
    return render(request, 'afloapp/pages/index.html', context)


def contact(request):
    return render(request, 'afloapp/pages/contact.html')


def formateurs(request):
    formateurs = Formateur.objects.all()
    context = { "formateurs": formateurs }
    return render(request, 'afloapp/pages/formateurs.html', context)