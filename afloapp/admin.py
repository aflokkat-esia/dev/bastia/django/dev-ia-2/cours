from django.contrib import admin
from . import models

# Register your models here.

admin.site.register(models.Formateur)
admin.site.register(models.Formation)
admin.site.register(models.Etudiant)
admin.site.register(models.Matiere)
